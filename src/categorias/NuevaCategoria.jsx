import React, { useState } from "react";
import { Redirect } from "react-router-dom";

import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import { Row, Col } from "reactstrap";

const NuevoContacto = (props) => {
  const [nombre, setNombre] = useState("");
  const [volver, setVolver] = useState(false);

  //método activado al enviar el form (submit)
  function guardar(e) {
    e.preventDefault();

    //validación de datos!

    props.nuevaCategoria({
      nombre,
    });

    setVolver(true);
  }

  function cancelar() {
    setVolver(true);
  }

  if (volver === true) {
    return <Redirect to="/categorias/lista" />;
  }

  return (
    <>
      <br />
      <h3>Nuevo contacto</h3>
      <Form onSubmit={guardar}>
        <Row>
          <Col xs="6">
            <FormGroup>
              <Label for="nombreInput">Nombre</Label>
              <Input
                type="text"
                id="nombreInput"
                value={nombre}
                onChange={(e) => setNombre(e.target.value)}
              />
            </FormGroup>
          </Col>
        </Row>

        <Row>
          <Col>
            <Button type="submit" color="primary">
              Guardar
            </Button>{" "}
            <Button type="button" onClick={cancelar} color="danger">
              Cancelar
            </Button>
          </Col>
        </Row>
      </Form>
    </>
  );
};

export default NuevoContacto;
