import React, { useState } from "react";
import { Redirect } from "react-router-dom";

import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import { Row, Col } from "reactstrap";

const ModificaCategoria = (props) => {
  const id = props.id * 1; //convertimos a int!!!
  const categoria = props.categorias.find((el) => el.id === id);

  const [nombre, setNombre] = useState(categoria.nombre);
  const [volver, setVolver] = useState(false);

  //método activado al enviar el form (submit)
  function guardar(e) {
    e.preventDefault();
    //validación de datos!
    props.guardaCategoria({
      id,
      nombre,
    });
    setVolver(true);
  }

  function cancelar() {
    setVolver(true);
  }

  if (volver === true) {
    return <Redirect to="/categorias/lista" />;
  }

  return (
    <>
      <br />
      <h3>Modifica categoria</h3>
      <Form onSubmit={guardar}>
        <Row>
          <Col xs="6">
            <FormGroup>
              <Label for="nombreInput">Nombre</Label>
              <Input
                type="text"
                id="nombreInput"
                value={nombre}
                onChange={(e) => setNombre(e.target.value)}
              />
            </FormGroup>
        
          </Col>
        </Row>

        <Row>
          <Col>
            <Button type="submit" color="primary">
              Guardar
            </Button>{" "}
            <Button type="button" onClick={cancelar} color="danger">
              Cancelar
            </Button>
          </Col>
        </Row>
      </Form>
    </>
  );
};

export default ModificaCategoria;
