import React, { useState } from "react";
import { Redirect } from "react-router-dom";

import { Button } from "reactstrap";

const EliminaCategoria = (props) => {
  const id = props.id * 1; //convertimos a int!!!
  const categoria = props.categorias.find((el) => el.id === id);
  const [volver, setVolver] = useState(false);

  function eliminar() {
    props.eliminaCategoria(id);
    setVolver(true);
  }

  function cancelar() {
    setVolver(true);
  }

  if (volver === true) {
    return <Redirect to="/categorias/lista" />;
  }

  return (
    <>
      <br />
      <h3>Desea eliminar a {categoria.nombre}?</h3>
      <br />
      <Button onClick={eliminar} color="danger">
        Sí
      </Button>{" "}
      <Button onClick={cancelar} color="success">
        No
      </Button>
    </>
  );
};

export default EliminaCategoria;
