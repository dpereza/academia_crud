import React, { useState } from "react";
import { Redirect } from "react-router-dom";

import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import { Row, Col } from "reactstrap";

const ModificaCategoria = (props) => {
  const { contactos, categorias, vincula } = props;

  const [contacto, setContacto] = useState("-");
  const [categoria, setCategoria] = useState("-");
  const [volver, setVolver] = useState(false);

  //método activado al enviar el form (submit)
  function vincular(e) {
    e.preventDefault();
    //validación de datos!
    vincula(categoria, contacto);
    setVolver(true);
  }

  function cancelar() {
    setVolver(true);
  }

  if (volver === true) {
    return <Redirect to="/" />;
  }

  const cats = categorias.map(el => <option key={el.id} value={el.id}>{el.nombre}</option>)
  const cons = contactos.map(el => <option key={el.id} value={el.id}>{el.nombre}</option>)

  return (
    <>
      <br />
      <h3>Vincula contacto categoria</h3>
      <Form onSubmit={vincular}>
        <Row>
          <Col xs="6">
            <FormGroup>
              <Label for="catsselect">Categoria</Label>
              <Input type="select"  id="catsselect" onChange={e => setCategoria(e.target.value)}>
                <option value={0}>-</option>
                {cats}
              </Input>
            </FormGroup>
          </Col>
          <Col xs="6">
            <FormGroup>
              <Label for="consselect">Contacto</Label>
              <Input type="select"  id="consselect" onChange={e => setContacto(e.target.value)}>
                <option value={0}>-</option>
                {cons}
              </Input>
            </FormGroup>
          </Col>
        </Row>

        <Row>
          <Col>
            <Button disabled={categoria==='-' || contacto==='-'} type="submit" color="primary">
              Guardar
            </Button>{" "}
            <Button type="button" onClick={cancelar} color="danger">
              Cancelar
            </Button>
          </Col>
        </Row>
      </Form>
    </>
  );
};

export default ModificaCategoria;
