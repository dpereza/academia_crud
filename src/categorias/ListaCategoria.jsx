import React, {useContext} from "react";
import TraductorContext from "../TraductorContext.js";

import { Table } from "reactstrap";
import { Link } from "react-router-dom";

const Lista = (props) => {
  const Traductor = useContext(TraductorContext);
  //para crear las filas hacemos un "map" de los categorias recibidos
  let filas = props.categorias
    .map((categoria) => {
      return (
        <tr key={categoria.id}>
          <td>{categoria.id}</td>
          <td>{categoria.nombre}</td>
          <td>
            <Link  to={"/categorias/modifica/" + categoria.id}><i className="fa fa-edit fa-2x"></i></Link>
          </td>
          <td>
            <Link  to={"/categorias/elimina/" + categoria.id}><i style={{color: "red"}} className="fa fa-trash fa-2x "></i></Link>
          </td>
          <td>
            <Link  to={"/categorias/" + categoria.id +"/contactos"}><i style={{color: "blue"}} className="fa fa-users fa-2x "></i></Link>
          </td>
        </tr>
      );
    });

  return (
    <>
    <br />
    <h3>{Traductor.traduce("categorias")}</h3>
    <Table>
      <thead>
        <tr>
          <th>#</th>
          <th>Nombre</th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>{filas}</tbody>
    </Table>
    <br />
    <Link className="btn btn-success" to="/categorias/nuevo">{Traductor.traduce("nuevo")}</Link>
    </>
  );
};

export default Lista;
