import React, {useContext, useState} from "react";
import TraductorContext from "../TraductorContext.js";
import { Redirect } from "react-router-dom";

import { Table, Button } from "reactstrap";

const Lista = (props) => {
  const Traductor = useContext(TraductorContext);

  const { categorias, contactos, categoriasContactos} = props;
  const id = props.id * 1;
  const [volver, setVolver] = useState(false);

  //para crear las filas hacemos un "map" de los contactos recibidos
  let filas = contactos
    .filter(el => 
      categoriasContactos.filter(el => el.categoria_id===id).map(el => el.contacto_id).includes(el.id)
    )
    .map((contacto) => {
      return (
        <tr key={contacto.id}>
          <td>{contacto.id}</td>
          <td>{contacto.nombre}</td>
          <td>{contacto.email}</td>
        </tr>
      );
    });


    
  if (volver === true) {
    return <Redirect to="/categorias/lista" />;
  }


  return (
    <>
    <br />
    <h3>{Traductor.traduce("contactos")} {categorias.find(el => el.id===id).nombre}</h3>
    <Table>
      <thead>
        <tr>
          <th>#</th>
          <th>Nombre</th>
          <th>Email</th>
        </tr>
      </thead>
      <tbody>{filas}</tbody>
    </Table>
    <br />
    <Button type="button" onClick={()=>setVolver(true)} color="primary">
              Volver
            </Button>
    </>
  );
};

export default Lista;
