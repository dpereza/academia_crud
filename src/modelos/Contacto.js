
const API = 'http://localhost:3000/api/contactos';
const HOST = 'localhost'

const HEADERS =  new Headers(
    {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Host': HOST
    });



async function getAll() {

    const opcionesFetch = {
        method: "GET",
        headers: HEADERS
    };
    
    const response = await fetch(API, opcionesFetch);
    if (response.ok) {
        const resp = await response.json();
        return resp;
    } else {
        return [];
    }
 }




 async function add(datos){

    const opcionesFetch =  {
        method: "POST",
        headers: HEADERS,
        body: JSON.stringify(datos)
    };
    
    const response = await fetch(API, opcionesFetch);

    return response.ok; // true/false

 }



 async function update(datos){

    const opcionesFetch =  {
        method: "PATCH",
        headers: HEADERS,
        body: JSON.stringify(datos)
    };
    
    const response = await fetch(API+"/"+datos.id, opcionesFetch);

    return response.ok; // true/false

 }


 async function remove(idEliminar){

    const opcionesFetch =  {
        method: "DELETE",
        headers: HEADERS,
    };
    
    const response = await fetch(API+"/"+idEliminar, opcionesFetch);

    return response.ok; // true/false

 }



const Contacto = {getAll, add, update, remove};

export default Contacto;

