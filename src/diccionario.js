
const diccionario = {
    titol: ['Mi agenda de contactos', 'La meva llibreta de contactes'],
    inici: ['Inicio', 'Casa'],
    contactos: ['Contactos', 'Contactes'],
    categorias: ['Categorias', 'Categories'],
    listado: ['Listado', 'Llistat'],
    nombre: ['Nombre', 'Nom'],
    email: ['Correo electrónico', 'Correu electrònic'],
    editar: ['Editar', 'Modifica'],
    eliminar: ['Eliminar', 'Esborra'],
    nuevo: ['Añadir registro', 'Afegeix registre'],
    volver: ['Volver', 'Torna'],
    tel: ['Teléfono', 'Telèfon'],
    cancelar: ['Cancelar', 'Deixa-ho estar'],
    guardar: ['Guardar', 'Desa els canvis'],
}

export default diccionario;