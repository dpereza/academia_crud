import React, { useState, useEffect } from "react";

import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container, Row, Col, Button } from 'reactstrap';

// importamos los componentes de la aplicación (vistas)
import Inicio from './Inicio';
import ListaContactos from './contactos/ListaContacto';
import NuevoContacto from './contactos/NuevoContacto';
import ModificaContacto from './contactos/ModificaContacto';
import EliminaContacto from './contactos/EliminaContacto';

import ListaCategorias from './categorias/ListaCategoria';
import NuevaCategoria from './categorias/NuevaCategoria';
import ModificaCategoria from './categorias/ModificaCategoria';
import EliminaCategoria from './categorias/EliminaCategoria';
import Vincula from './categorias/Vincula';

import ListaContactosCategoria from './categorias/ListaContactosCategoria';

import P404 from './P404';

// importamos css
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './App.css';

// contexto donde guardaremos la función traduce y el idioma actual
import TraductorContext from "./TraductorContext";

// datos y contexto para traducciones
import diccionario from "./diccionario";

//
import ContactoModel  from './modelos/Contacto';
import CategoriaModel  from './modelos/Categoria';
import CategoriaContactoModel from './modelos/CategoriaContacto';

//m8sWZ2nMNstKVcAF6eJX 


// clase App 
const App = () => {

  const [contactos, setContactos] = useState([]);
  const [categorias, setCategorias] = useState([]);
  const [categoriasContactos, setCategoriasContactos] = useState([]);
  const [idioma, setIdioma] = useState(0);

  // función traduce, devuelve el string correspondiente a la etiqueta e idioma facilitados
  const traduce = (etiqueta) => diccionario[etiqueta] ? diccionario[etiqueta][idioma] : `*${etiqueta}*`;



  async function loadData() {
    let cons = await ContactoModel.getAll();
    let cats = await CategoriaModel.getAll();
    let catscons = await CategoriaContactoModel.getAll();
    setContactos(cons);
    setCategorias(cats);
    setCategoriasContactos(catscons);
  }

  //contactos
  async function guardaContacto(contacto) {
    let x = await ContactoModel.update(contacto);
    // console.log(x ? "ok modificar" : "error modificar");
    loadData();
  }
  async function nuevoContacto(contacto) {
    let x = await ContactoModel.add(contacto);
    // console.log(x ? "ok nuevo" : "error nuevo");
    loadData();
  }
  async function eliminaContacto(idEliminar) {
    let x = await ContactoModel.remove(idEliminar);
    // console.log(x ? "ok eliminar" : "error eliminar");
    loadData();
  }


  //categorias
  async function guardaCategoria(categoria) {
    let x = await CategoriaModel.update(categoria);
    // console.log(x ? "ok modificar" : "error modificar");
    loadData();
  }
  async function nuevaCategoria(categoria) {
    let x = await CategoriaModel.add(categoria);
    // console.log(x ? "ok nuevo" : "error nuevo");
    loadData();
  }
  async function eliminaCategoria(idEliminar) {
    let x = await CategoriaModel.remove(idEliminar);
    // console.log(x ? "ok eliminar" : "error eliminar");
    loadData();
  }

  async function vincula(categoria_id, contacto_id) {
    let x = await CategoriaContactoModel.add({categoria_id, contacto_id});
    console.log(x ? "ok vincular" : "error vincular");
    loadData();
  }


  useEffect(() => {
    loadData();
  }, [])

  return (
    <TraductorContext.Provider value={{ traduce, idioma }}>
      <BrowserRouter>
        <br />
        <br />
        <Container>
          <Row>
            <Col>
              <ul className="list-unstyled menu">
                <li> <Link className="link" to="/">Inicio</Link> </li>
                <li> <Link className="link" to="/contactos/lista">Contactos</Link> </li>
                <li> <Link className="link" to="/categorias/lista">Categorias</Link> </li>
                <li> <Link className="link" to="/vincula">Vincula</Link> </li>
              </ul>
            </Col>
            <Col className="text-right">
              <Button
                size="sm"
                color="primary"
                outline={idioma === 0 ? false : true}
                onClick={() => setIdioma(0)}
              >
                ES
              </Button>{" "}
              <Button
                size="sm"
                color="primary"
                outline={idioma === 1 ? false : true}
                onClick={() => setIdioma(1)}
              >
                CA
              </Button>
            </Col>
          </Row>
          <Row>
            <Col>
              <Switch>
                <Route exact path="/" component={Inicio} />
                <Route path="/contactos/lista" render={() => <ListaContactos contactos={contactos} />} />
                <Route path="/contactos/nuevo" render={() => <NuevoContacto nuevoContacto={nuevoContacto} />} />
                <Route path="/contactos/modifica/:id" render={(props) => <ModificaContacto id={props.match.params.id} contactos={contactos} guardaContacto={guardaContacto} />} />
                <Route path="/contactos/elimina/:id" render={(props) => <EliminaContacto id={props.match.params.id} contactos={contactos} eliminaContacto={eliminaContacto} />} />
                <Route path="/categorias/lista" render={() => <ListaCategorias categorias={categorias} />} />
                <Route path="/categorias/nuevo" render={() => <NuevaCategoria nuevaCategoria={nuevaCategoria} />} />
                <Route path="/categorias/modifica/:id" render={(props) => <ModificaCategoria id={props.match.params.id} categorias={categorias} guardaCategoria={guardaCategoria} />} />
                <Route path="/categorias/elimina/:id" render={(props) => <EliminaCategoria id={props.match.params.id} categorias={categorias} eliminaCategoria={eliminaCategoria} />} />
                <Route path="/categorias/:id/contactos" render={(props) => <ListaContactosCategoria id={props.match.params.id} categorias={categorias} contactos={contactos} categoriasContactos={categoriasContactos} />} />
                <Route path="/vincula" render={(props) => <Vincula categorias={categorias} contactos={contactos} vincula={vincula} />} />
                <Route component={P404} />
              </Switch>
            </Col>
          </Row>


        </Container>
      </BrowserRouter>
    </TraductorContext.Provider>

  );

}

export default App;